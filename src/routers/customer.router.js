const { Router } = require("express");

const router = Router();

const middleware = function() {
    console.log("Soy un middleware")
}

router.get("/", [middleware], (req, res) => {
    res.send("Aqui se listan los clientes");
});

router.post("/", (req, res) => {
    res.send("Aquí se agregan los clientes");
});

router.delete("/:id", (req, res) => {
    res.send("Aquí se eliminan los clientes");
});

router.put("/:id", (req, res) => {
    res.send("Aquí se actulizan los clientes");
});

module.exports = router;