const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const router = require("./routers/customer.router")

const app = express();

app.use(cors());
app.use(bodyParser.json());

app.use("/customer", router);

const port = 4000;

app.listen(port, () => {
    console.log(`Server is running on port: http://localhost:${port}`)
});